import appdaemon.plugins.hass.hassapi as hass

class Waschmaschine(hass.Hass):

  VALUE_ARBEITSPREIS = "static.arbeitspreis"

  SENSOR_WASCHMASCHINE = "sensor.waschmaschine"
  SENSOR_POWER = "sensor.leistung"
  SENSOR_USAGE = "sensor.gesamter_verbrauch"
  
  COSTS_kWh = 26.63 #cent/kWh

  timer = {}

  def initialize(self):
    self.log("Starting Waschmaschine automation")
    self.listen_state(self.power, self.SENSOR_POWER)
    
    self.listen_state(self.tracker, self.SENSOR_POWER)

    self.set_state(self.VALUE_ARBEITSPREIS, state = self.COSTS_kWh, attributes = {"friendly_name": "Arbeitspreis", "icon": "mdi:currency-eur", "unit_of_measurement": "cent/kWh"})
    self.set_state(self.SENSOR_WASCHMASCHINE, state = "off", attributes = {"friendly_name": "Waschmaschine", "icon": "mdi:washing-machine"})
    self.log("Loaded Arbeitspreis: {} cent/kWh".format(self.COSTS_kWh))

  def power(self, entity, attribute, old, new, kwargs):
    self.log("Entity: {}, attribute: {}, old: {}, new: {}".format(entity, attribute, old, new))
    oldInt = int(old)
    newInt = int(new)

    usage = self.get_state(self.SENSOR_USAGE)

    self.log("Entity: {}, state: {}".format(self.SENSOR_USAGE, usage))

    if oldInt == 0 and newInt > 0:
      self.log("Waschmaschine turned on")
      self.set_state(self.SENSOR_WASCHMASCHINE, state = "on", attributes = {"start_usage": usage})
    elif oldInt == 2 and newInt > 2: 
      self.log("Waschmaschine start running")
      self.set_state(self.SENSOR_WASCHMASCHINE, state = "running")
    elif oldInt > 0 and newInt == 0:
      self.log("Waschmaschine turned off")
      self.set_state(self.SENSOR_WASCHMASCHINE, state = "off", attributes = {"end_usage": usage})

  def tracker(self, entity, attribute, old, new, kwargs):
     oldInt = int(old)
     newInt = int(new)

     self.cancel_timer(self.timer)

     if oldInt > 2 and newInt == 2:
       self.log("Starting timer")
       self.timer = self.run_in(self.change_status, seconds=120)
  
  def change_status(self, seconds):
     usage = self.get_state(self.SENSOR_USAGE)
     start_usage = float(self.get_state(self.SENSOR_WASCHMASCHINE, attribute = "start_usage"))  
     costs = (float(usage) - start_usage) * float(self.COSTS_kWh)
     self.log("Waschmachine finished. start_usage: {}, end_usage: {}, costs: {}".format(start_usage, usage, costs))
     self.set_state(self.SENSOR_WASCHMASCHINE, state = "finished", attributes = {"end_usage": usage, "last_costs": costs})


